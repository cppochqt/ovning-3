#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Koppla Arkiv->Exit till avsluta
    connect(ui->actionExit, SIGNAL(triggered(bool)), QApplication::instance(), SLOT(quit()));

    // Koppla alla räknare till rätt event
    connect(ui->pEditor, SIGNAL(textChanged()), this, SLOT(raknaTecken()));
    connect(ui->pEditor, SIGNAL(textChanged()), this, SLOT(raknaMellanslag()));
    connect(ui->pEditor, SIGNAL(textChanged()), this, SLOT(raknaBokstaver()));

    // Skriv nollor i alla räknare
    raknaTecken();
    raknaMellanslag();
    raknaBokstaver();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::raknaTecken()
{
    QString text = ui->pEditor->toPlainText();
    int antal = 0;

    // Räkna alla tecken utom returtecken
    for(int index = 0; index < text.size(); index++)
    {
        if (text[index] != '\n')
            antal++;
    }

    ui->pTecken->setText(QString("%1").arg(antal));

}

void MainWindow::raknaMellanslag()
{
    QString text = ui->pEditor->toPlainText();
    int antal = 0;

    // Uppgift 1: Räkna alla tecken som är mellanslag


    ui->pMellanslag->setText(QString("%1").arg(antal));
}

void MainWindow::raknaBokstaver()
{
    QString text = ui->pEditor->toPlainText();
    int antal = 0;

    // Uppgift 2: Räkna alla bokstäver

    ui->pBokstaver->setText(QString("%1").arg(antal));
}
