# README

Detta är Övning 3 i Eriks kurs i C++ och Qt för Nybörjare

## Kursen som helhet
Kursen är framtagen i syfte att lära en specifik person C++ och Qt. Under kursen försöker jag använda enkel Svenska. Övningar skrivs efter hand, om det blir en eller många får framtiden utvisa. 

## Övningens syfte
Övningen är första mötet med Qt och skall skapa en första orientering hur det kan se ut. Huvudsyftet är dock att genomföra en första övning med for-loopar och if-satser. 

## Ärttigheter och Licens
Källkoden är (C) 2018 Erik Ridderby men fritt tillgänglig under Erkännande-DelaLika 4.0 Internationell (CC BY-SA 4.0). Nedan finns en sammanfattning men en [fullständig version av licensen finns på Creative Commons hemsida](https://creativecommons.org/licenses/by-sa/4.0/legalcode.sv)

### Du har tillstånd att:
Dela — kopiera och vidaredistribuera materialet oavsett medium eller format

Bearbeta — remixa, transformera, och bygg vidare på materialet för alla ändamål, även kommersiellt.

Licensgivaren kan inte återkalla dessa friheter så länge du följer licensvillkoren.

### På följande villkor:
Erkännande — Du måste ge ett korrekt erkännande, ange en hyperlänk till licensen, och ange om bearbetningar är gjorda . Du behöver göra så i enlighet med god sed, och inte på ett sätt som ger en bild av att licensgivaren stödjer dig eller ditt användande.

DelaLika — Om du remixar, transformerar eller bygger vidare på materialet måste du distribuera dina bidrag under samma licens som originalet. 


Inga ytterligare begränsningar — Du får inte tillämpa lagliga begränsningar eller teknologiska metoder som juridiskt begränsar andra från att gör något som licensen tillåter. 

![CC-BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)